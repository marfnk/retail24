# Retail24

A nice coding kata.

## Setup and Run

This is a standard Node.js project.

- `npm install`
- `npm run build`
- `npm run start`

## Tests

To run the tests, execute:

- `npm run test`

## Usage

Use any REST client and make a call to:

- `GET http://localhost:3000/api/transactions`

Please specify a `transactionId` and a `confidenceLevel`.

Example: `GET http://localhost:3000/api/transactions?transactionId=5c868b22eb7069b50c6d2d32&confidenceLevel=0.8`

## Deployment

This app uses a Gitlab CI/CD pipeline and deploys the artifacts to Heroku.
Simply push or merge to `master` to trigger the pipeline. 
