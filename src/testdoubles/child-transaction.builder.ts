import { ChildTransaction } from '../transaction/transaction.model';

export class ChildTransactionBuilder {
  private constructor(private childTransaction: ChildTransaction) {
  }

  public static get new(): ChildTransactionBuilder {
    return new ChildTransactionBuilder({
      id: '1',
      age: 32,
      name: 'Sanchez Collier',
      email: 'sanchezcollier@equicom.com',
      phone: '(897) 421-3152',
      geoInfo: {
        latitude: -66.117512,
        longitude: -50.147742
      },
      connectionInfo: {
        type: 'sameGeoInfo',
        confidence: 0.8
      },
      children: []
    });
  }

  public withId(id: string) {
    this.childTransaction.id = id;
    return this;
  }

  withConfidence(confidence: number) {
    this.childTransaction.connectionInfo.confidence = confidence;
    return this;
  }

  withConnectionType(type: string) {
    this.childTransaction.connectionInfo.type = type;
    return this;
  }

  public withChildren(children: ChildTransaction[]) {
    this.childTransaction.children = children;
    return this;
  }

  public withoutConnectionInfo() {
    delete this.childTransaction.connectionInfo;
    return this;
  }

  public build(): ChildTransaction {
    return this.childTransaction;
  }

}
