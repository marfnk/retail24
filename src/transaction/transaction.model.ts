export interface BaseTransaction {
  id: string;
  age: number;
  name: string;
  email: string;
  phone: string;
  geoInfo: {
    latitude: number,
    longitude: number
  }
}

interface WithConnectionInfo {
  // There are child transactions without connectionInfo (eg. 5c868b9b6c4fc9fe97f61d7b)
  // I'll treat them as "corrupt data" and not connected to the parent.
  // I guess, this is an error in the example data set.
  connectionInfo?: {
    type: string;
    confidence: number;
  },
}

interface WithChildren {
  // There are some transactions with out the children property (eg. 5c868b22d84354bef2474acb)
  // I'll treat them the same as with an empty array of children.
  // I guess, this is an error in the example data set.
  children?: ChildTransaction[];
}

/**
 * TopLevelTransaction are transactions with children but no connection info
 */
export type TopLevelTransaction = BaseTransaction & WithChildren;

/**
 * ChildTransactions are transactions with children (?) and connectionInfo
 */
export type ChildTransaction = BaseTransaction & WithChildren & WithConnectionInfo;

/**
 * FlatTransactions are transactions with connectionInfo, combinedConnectionInfo but no children
 */
export interface FlatTransaction extends BaseTransaction, WithConnectionInfo {
  combinedConnectionInfo: {
    types: string[];
    confidence: number;
  }
}
