import { app } from '../server';
import * as supertest from 'supertest';
import * as HttpStatus from 'http-status-codes';

describe('Transaction Endpoint', () => {

  it('should return 400 BAD REQUEST for missing transaction IDs', async done => {
    const request = supertest(app);
    const res = await request.get('/api/transactions').query({confidenceLevel: 1});

    expect(res.status).toEqual(HttpStatus.BAD_REQUEST);
    done();
  });

  it('should return 400 BAD REQUEST for missing confidenceLevel', async done => {
    const request = supertest(app);
    const res = await request.get('/api/transactions').query({transactionId: '1.1'});

    expect(res.status).toEqual(HttpStatus.BAD_REQUEST);
    done();
  });

  it('should not return a transaction with too little confidence', async done => {
    const request = supertest(app);
    const res = await request.get('/api/transactions').query({
      transactionId: '5c868b224aafffc5fcffd9c3',
      confidenceLevel: 0.9
    });

    expect(res.status).toEqual(HttpStatus.NOT_FOUND);
    done();
  });

  it('should not return a matching transaction', async done => {
    const request = supertest(app);
    const res = await request.get('/api/transactions').query({
      transactionId: '5c868b224aafffc5fcffd9c3',
      confidenceLevel: 0.8
    });

    expect(res.status).toEqual(HttpStatus.OK);
    done();
  });

  it('should treat a missing confidence as 1', async done => {
    const request = supertest(app);
    const res = await request.get('/api/transactions').query({
      transactionId: '5c868b9b1025c3dee1dbc4fc',
      confidenceLevel: 1
    });

    expect(res.status).toEqual(HttpStatus.OK);
    done();
  });

});
