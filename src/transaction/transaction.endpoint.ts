import * as express from 'express';
import { Request } from 'express';
import * as HttpStatus from 'http-status-codes';
import {
  extractConfidenceFromQueryParam,
  extractTransactionIdFromQueryParam,
  ResponseWithTypedLocals
} from './query-param.extractor';
import { TransactionDatabase } from './transaction.database';
import { ChildTransaction } from './transaction.model';
import { flattenTransaction } from './transaction.utils';

export class TransactionEndpoint {
  private transactionDatabase: TransactionDatabase;

  constructor(app: express.Application) {
    this.transactionDatabase = new TransactionDatabase();

    app.get('/api/transactions',
      extractTransactionIdFromQueryParam,
      extractConfidenceFromQueryParam,
      this.getTransactions.bind(this)
    );
  }

  private getTransactions(req: Request, res: ResponseWithTypedLocals) {
    const transactionId: string = res.locals.transactionId;
    const confidenceLevel: number = res.locals.confidenceLevel;
    const transaction = this.transactionDatabase.loadTransactionById(transactionId);

    // if transaction has a connection info, take the connectionInfo confidence, else use 1 (see requirement 1.d)
    const transactionConfidence = ((<ChildTransaction> transaction).connectionInfo || {}).confidence || 1;
    if (!transaction || transactionConfidence < confidenceLevel) {
      return res.status(HttpStatus.NOT_FOUND).send('Not transactions are matching your search criteria.');
    }

    return res.send(flattenTransaction(transaction));
  }

}
