import { TransactionDatabase } from './transaction.database';

jest.mock('../test-data.json', () => [
  {id: '1'},
  {
    id: '2',
    children: [
      {
        id: '2.1',
        children: [
          {id: '2.1.1'},
          {id: '2.1.2'},
          {
            id: '2.1.3',
            children: [
              {id: '2.1.3.1'}
            ]
          }
        ]
      },
      {id: '2.2'}
    ]
  }
]);

describe('Transaction Database', () => {
  let database: TransactionDatabase;

  beforeEach(() => {
    database = new TransactionDatabase();
  });

  it('should return the sample data from the json file', () => {
    const transactions = database.loadAllTransactions();

    expect(transactions.length).toBe(2);
    expect(transactions[0].id).toBe('1');
  });

  describe('should find transactions by ID', () => {

    it('for top level transactions', () => {
      let transaction = database.loadTransactionById('1');

      expect(transaction).toBeDefined();
      expect(transaction.id).toBe('1');

      transaction = database.loadTransactionById('2');
      expect(transaction.id).toBe('2');
    });

    it('for nested transactions level 2', () => {
      const transaction = database.loadTransactionById('2.2');

      expect(transaction).toBeDefined();
      expect(transaction.id).toBe('2.2');
    });

    it('for nested transactions level 3', () => {
      const transaction = database.loadTransactionById('2.1.3');

      expect(transaction).toBeDefined();
      expect(transaction.id).toBe('2.1.3');
    });
  });

});
